using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ValidateScopesExample
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseDefaultServiceProvider(serviceProviderOptions => 
                    {
                        // Verify that no scoped service is requested from the root provider
                        serviceProviderOptions.ValidateScopes = true;
                        // Nice to know: If services are missing, fail on provider build time, instead on runtime
                        serviceProviderOptions.ValidateOnBuild = true;
                    }
                )
                .ConfigureServices(services =>
                    {
                        services.AddHostedService<Worker>();
                        services.AddScoped<ScopedService>();
                    }
                );
    }

    public class Worker : BackgroundService
    {
        private readonly IHostApplicationLifetime applicationLifetime;

        public Worker(IServiceProvider serviceProvider, IHostApplicationLifetime applicationLifetime)
        {
            this.applicationLifetime = applicationLifetime;
            // Request the ScopedService from the root provider
            serviceProvider.GetRequiredService<ScopedService>();
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            // In case we come this far, just stop the app right away
            applicationLifetime.StopApplication();

            return Task.CompletedTask;
        }
    }

    public class ScopedService
    {
        public ScopedService()
        {
            Console.WriteLine("ScopedService constructed");
        }
    }
}
