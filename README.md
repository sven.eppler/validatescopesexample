# ValidateScopesExample

A simple .net core 3.1 worker service example which shows, how to enable scope validation for the `IServiceProvider` dependency injection provider in .net core.

## Overview

The application consists of these basic classes which are all included in the file `Program.cs`:

* `Program`: The default class wich includes the entry point main function. It creates the `IHost` and configures the services and the service provider.
* `Worker`: A simple implementation for a `BackgroundService` which is requesting a scoped service from the root provider
* `ScopedService`: Dummy service which is added to the DI-Container with the `Scoped`-Lifetime

## Configuring the IServiceProvider
In order to configure the `IServiceProvider` you can use the [UseDefaultServiceProvider](https://docs.microsoft.com/en-us/dotnet/api/microsoft.extensions.hosting.hostinghostbuilderextensions.usedefaultserviceprovider?view=dotnet-plat-ext-5.0#Microsoft_Extensions_Hosting_HostingHostBuilderExtensions_UseDefaultServiceProvider_Microsoft_Extensions_Hosting_IHostBuilder_System_Action_Microsoft_Extensions_DependencyInjection_ServiceProviderOptions__) extension method.

See [Program.cs:18](https://gitlab.com/sven.eppler/validatescopesexample/-/blob/main/Program.cs#L18)

## Enable VerifyScope
When setting the property [VerifyScope](https://docs.microsoft.com/en-us/dotnet/api/microsoft.extensions.dependencyinjection.serviceprovideroptions.validatescopes?view=dotnet-plat-ext-5.0&viewFallbackFrom=netcore-3.1#Microsoft_Extensions_DependencyInjection_ServiceProviderOptions_ValidateScopes) to `true` the `IServiceProvider` will check if the requested service is a scoped service and it is requested by the "root provider". If so, a `InvalidOperationException` will be thrown.

```
System.InvalidOperationException: Cannot resolve scoped service 'ValidateScopesExample.ScopedService' from root provider.
```

See [Program.cs:21](https://gitlab.com/sven.eppler/validatescopesexample/-/blob/main/Program.cs#L21)

## Enable ValidateOnBuild
Another usefull options is to set [ValidateOnBuild](https://docs.microsoft.com/en-us/dotnet/api/microsoft.extensions.dependencyinjection.serviceprovideroptions.validateonbuild?view=dotnet-plat-ext-5.0) to `true`. This tells the `IServiceProvider` to check that all services with all dependencies are registered at provider built time. Normally this only leads to exceptions at runtime when a service is requested for which a dependency cannot be found.

See [Program.cs:23](https://gitlab.com/sven.eppler/validatescopesexample/-/blob/main/Program.cs#L23)

## Check existing applications without changing the code
You can enable `VerifyScope` by simply setting the environment variable `DOTNET_ENVIRONMENT` to `Development` when starting a .net core application.

But it will **NOT** enable `ValidateOnBuild`!

## Execute the sample
Well, it's a .net core worker service. Just type `dotnet run`. :)